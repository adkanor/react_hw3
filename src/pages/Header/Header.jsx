import CartIconForHeader from "./BtnsForHeader/CartBtnForHeader/Cart";
import FavouriteIconForHeader from "./BtnsForHeader/FavouriteBtnForHeader/FavoriteBtn";
import GoBackIcon from "./BtnsForHeader/GoBackIcon/GoBackIcon";
import "./Header.scss";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

function Header({ cartAmount, favoriteAmount }) {
  return (
    <div className="header">
      <div className="header__links">
        <Link to="/" className="header__link">
          <div className="header__link-to-main">
            <GoBackIcon /> Main page
          </div>
        </Link>
        <div className="header__links-group">
          <Link to="/cart" className="header__link">
            <CartIconForHeader cartAmount={cartAmount} />
          </Link>
          <Link to="/favourites" className="header__link">
            <FavouriteIconForHeader favoriteAmount={favoriteAmount} />
          </Link>
        </div>
      </div>
    </div>
  );
}
Header.propTypes = {
  cartAmount: PropTypes.number.isRequired,
  favoriteAmount: PropTypes.number.isRequired,
};
export default Header;
