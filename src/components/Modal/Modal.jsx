import "./Modal.scss";
import PropTypes from "prop-types";
function Modal({
  active,
  header,
  textContent,
  closeModalWindow,
  submitBtnFunc,
}) {
  return (
    <div
      className={active ? "modal active" : "modal"}
      onClick={closeModalWindow}
    >
      <div className="modal_content" onClick={(e) => e.stopPropagation()}>
        <div className="modal-closeButton toClose" onClick={closeModalWindow}>
          X
        </div>
        <h2 className="modal-header">{header}</h2>
        <p className="modalTextContent">{textContent}</p>

        <div className="btnsForModal">
          <button
            className="actionForModal submit"
            onClick={() => {
              closeModalWindow();
              submitBtnFunc();
            }}
          >
            OK
          </button>
          <button className="actionForModal cancel" onClick={closeModalWindow}>
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  active: PropTypes.bool.isRequired,
  header: PropTypes.string.isRequired,
  textContent: PropTypes.string.isRequired,
  closeModalWindow: PropTypes.func.isRequired,
  submitBtnFunc: PropTypes.func.isRequired,
};
export default Modal;
