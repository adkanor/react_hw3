import "./AddToCart.scss";
import PropTypes from "prop-types";

function AddToCart({
  backgroundColor = "rgb(116, 34, 50)",
  text,
  openModalfunc,
  addToCartFunc,
}) {
  const bgColor = {
    backgroundColor: backgroundColor,
  };

  return (
    <button
      className="button"
      style={bgColor}
      onClick={() => {
        addToCartFunc();
        openModalfunc();
      }}
    >
      {text}
    </button>
  );
}

AddToCart.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  openModalfunc: PropTypes.func.isRequired,
  addToCartFunc: PropTypes.func.isRequired,
};

export default AddToCart;
