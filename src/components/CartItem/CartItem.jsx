import PropTypes from "prop-types";
import "./CartItem.scss";
import AddToCart from "../AddToCartBtn/AddToCart";

function CartItem(props) {
  const {
    id,
    tryToAddToCart,
    btnMessage,
    name,
    image,
    vendorCode,
    price,
    toOpenModal,
  } = props;

  const thisCard = { id, name, price, image, vendorCode };

  const tryToAddProdToCart = () => {
    tryToAddToCart(thisCard);
  };
  return (
    <div className="cartItem">
      <div className="cartItem__img">
        <img
          className="cartItem__image"
          src={image}
          alt="card of product in the cart"
        ></img>
      </div>
      <div className="cartItem__info">
        <p className="cartItem__name">{name} </p>
        <p className="cartItem__vendorCode">Сode: {vendorCode}</p>
        <p className="cartItem__price">
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
            currencyDisplay: "narrowSymbol",
          }).format(price)}
        </p>
      </div>
      <div className="cartItem__action">
        <AddToCart
          backgroundColor="rgb(116, 34, 50)"
          text={btnMessage}
          openModalfunc={toOpenModal}
          addToCartFunc={tryToAddProdToCart}
        />
      </div>
    </div>
  );
}
CartItem.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  vendorCode: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  btnMessage: PropTypes.string.isRequired,
  tryToAddToCart: PropTypes.func.isRequired,
  toOpenModal: PropTypes.func.isRequired,
};
export default CartItem;
